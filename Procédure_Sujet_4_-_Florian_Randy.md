---
title: "SAE 3.03 - Déploiement d'une application"
subtitle: "Procédure du Sujet 4"
author: "Randy De Wancker et Florian Delbe"
---

# 1. Installation d'Element

## Choix du serveur : Nginx

Pour installer Element Web, on privilégiera le serveur nginx, car il est plus performant pour gérer les requêtes sur des fichiers statiques.

De plus, nous l'avons déjà installé lors de la précédente procédure (sinon executer `# apt-get install nginx`).

En exécutant la commande `curl http://localhost`, on doit voir affichée la page d'accueil html.

## Téléchargement et configuration d'Element

Voici les commandes à executer sur la vm pour récupérer l'archive d'Element et la décompresser  :

```
$ wget https://github.com/vector-im/element-web/releases/download/v1.11.16/element-v1.11.16.tar.gz
$ tar -xf element-v1.11.16.tar.gz
```

On déplace ensuite les fichiers dans le dossier d'hébergement d'Nginx (en renommant le fichier `element`)
```
$ sudo mv element-v1.11.16.tar.gz /var/www/html/element
```

Une fois les fichiers déplacés, il est temps d'ajouter le fichier de configuration :
```
$ cd /var/www/html/element
$ cp config.sample.json config.json
$ nano config.json
```

## Configuration de Nginx

Pour pouvoir accéder à Element sur le port `8080`, on modifie la configuration d'Nginx dans `/etc/nginx/`
```
$ sudo nano /etc/nginx/nginx.conf
```

Dans la section `http {...}` on ajoute :
```
http {
  
  server {
    listen localhost:8080;
    root /var/www/html/element;
    index index.html;
  }

  ...

}
```
Puis on redémarre nginx pour appliquer les modifications.
```
$ sudo systemctl restart nginx
```

En faisant un `curl localhost:8080`, on doit tomber sur la page d'accueil element.

## Redirection SSH

Afin d'accéder à Element depuis la machine physique, il faut ajouter un tunnel ssh.
On sort de la machine virtuelle et on ouvre le fichier de configuration `.ssh/config`, où on ajoute :

```
Host element
  User user
  HostName 192.168.194.3
  LocalForward 0.0.0.0:9090 Localhost:8008 # (on garde le tunnel ssh du serveur synapse)
  LocalForward 0.0.0.0:8080 Localhost:8080
```

On peut ainsi accéder à Element dans un navigateur avec le lien `http://virt:8080`

# 2. Reverse Proxy

## Choix du reverse proxy

Il existe plusieurs solutions pour mettre en place un reverse proxy, comme Apache, Nginx, HAproxy, ou Squid par exemple.
Par question d'habitude, nous allons choisir Nginx comme reverse proxy.

## Creation et configuration de la nouvelle machine virtuelle : rproxy

Pour créer le reverse proxy, on commence par créer et configurer une nouvelle vm sur la machine de virtualisation avec la bonne ip et pour qu'elle soit accessible en ssh.
```
virt$ vmiut make rproxy
virt$ vmiut start rproxy
virt$ vmiut console rproxy
```
Se connecter en `root`
```
rproxy# ifdown enp0s3
rproxy# nano /etc/network/interfaces
```

Dans le fichier `interfaces` ajouter :
```
iface enp0s3 inet static
        address 192.168.194.4
        gateway 192.168.192.2
        netmask 255.255.255.0
```

Rétablir l'interface
```
rproxy# ifup enp0s3
```

Ajouter dans le fichier `/etc/environment`
```
HTTP_PROXY=http://cache.univ-lille.fr:3128
HTTPS_PROXY=http://cache.univ-lille.fr:3128
http_proxy=http://cache.univ-lille.fr:3128
https_proxy=http://cache.univ-lille.fr:3128
NO_PROXY=localhost,192.168.194.0/24,172.18.48.0/22
```

## Installation de Nginx en tant que reverse proxy

```
rproxy# apt-get update
rproxy# apt-get install nginx
```

Créer le fichier `/etc/nginx/sites-available/reverse-proxy.conf` avec ce contenu :
```
server {
  listen 80;
  location / {
    proxy_pass http://192.168.194.3:8080;
  }

}
```

# Configuration ssh

Dans le fichier de configuration `.ssh/config` de la machine de virtualisation on modifie la section `element` et on ajoute la section `rproxy` :
```
Host element
  User user
  HostName 192.168.194.3

Host rproxy
  User user
  HostName 192.168.194.4
  LocalForward 0.0.0.0:8080 localhost:80
```