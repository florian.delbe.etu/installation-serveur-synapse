---
title: "SAE 3.03 - Déploiement d'une application"
subtitle: "Procédure du Sujet 3"
author: "Randy De Wancker et Florian Delbe"
---

# 1. Installation d'un Service HTTP

## Installation du serveur et client HTTP sur la VM

Sur la machine virtuelle en `root`, installer `nginx` et `curl` avec ces commandes :
```
# apt-get install nginx
# systemctl status nginx #pour vérifier que le service est démarré
# apt-get install curl
```

En exécutant la commande `curl http://localhost`, on doit voir afficher la page d'accueil html.

## Accéder depuis la machine de virtualisation

En revanche, depuis la machine de virtualisation, exécuter simplement `curl 192.168.194.3` renvoie une page html d'erreur de connexion.<br/>
Pour remédier à ce problème, il faut préciser qu'on utilise pas de proxy avec l'option `-noproxy` suivi des adresses qui n'utilisent pas de proxy.<br/>
On arrive alors à se connecter avec la commande suivante :
```
$ curl -noproxy '*' 192.168.194.3
(ou $ curl -noproxy '192.168.194.3' 192.168.194.3)
```

## Accéder depuis la machine physique

Il n'est pas possible d'accéder directement au service nginx sur la vm à partir de la machine physique, car le service ne se trouve pas sur le même réseau. Pour y arriver, il faut donc passer par un tunnel ssh.<br/>
On crée alors un tunnel ssh entre la machine de virtualisation et la vm :
```
user@virt$ ssh -L 0.0.0.0:8080:localhost:80 vm
```

Pour être plus efficace, on crée un alias `vmtunnel` qui se charge de créer le tunnel ssh à chaque connexion vers la vm. Dans le fichier `.ssh/config` de la machine de virtualisation on ajoute :
```
Host vmtunnel
        User user
        HostName 192.168.194.3
        LocalForward 0.0.0.0:8080 localhost:80
```

Ainsi il est possible de se connecter depuis la machine physique à `acajou09.iutinfo.fr:8080` avec curl ou dans un navigateur.

# 2. Installation de Synapse

Pour installer Synapse sur la machine virtuelle, il faut suivre la <a href="https://matrix-org.github.io/synapse/latest/setup/installation.html#matrixorg-packages">procédure d'installation pour Debian</a>.

## Configuration de Postgres sur Synapse

Pour utiliser postgres sur synapse, il faut suivre la <a href="https://matrix-org.github.io/synapse/latest/postgres.html">procédure de configuration postgres</a>.

Ensuite, on recrée une base de données avec les bons paramètres.
```
$ sudo -u postgres bash
$ dropdb matrix
$ createdb --encoding=UTF8 --locale=C --template=template0 --owner=matrix matrix
```

On modifie le fichier `/etc/matrix-synapse/homeserver.yaml` en tant que `root`:
```
database:
  name: psycopg2
  args:
    user: matrix
    password: matrix
    database: matrix
    host: localhost
    cp_min: 5
    cp_max: 10
log_config: "/etc/matrix-synapse/log.yaml"
media_store_path: /var/lib/matrix-synapse/media
signing_key_path: "/etc/matrix-synapse/homeserver.signing.key"
trusted_key_servers: []
```

Puis on peut redémarrer synapse pour appliquer les modifications :
```
# systemctl restart matrix-synapse.service
```

## Ajouter des utilisateurs

On ajoute en bas du fichier `/etc/matrix-synapse/homeserver.yaml` :
```
registration_shared_secret: "toto" # Le code qui va servir à créer des utilisateurs
enable_registration: true
enable_registration_without_verification: true
```
Les deux dernières lignes permettront d'enregistrer de nouveaux utilisateurs, sans vérification, à partir du client element.

On peut créer un utilisateur avec cette commande :
```
# register_new_matrix_user -c /etc/matrix-synapse/homeserver.yaml
```

## Se connecter sur le serveur

Avant de se connecter sur le serveur de la machine virtuelle, il faut créer un tunnel dans le fichier `.ssh/config`
```
Host synapse
        User user
        HostName 192.168.194.3
        LocalForward 0.0.0.0:9090 localhost:8008
```
Puis on se connecte à la machine de virtualisation avec `ssh synapse`.

Pour commencer à discuter sur le serveur, on se connecte à l'adresse
`http://tp.iutinfo.fr:8888` où le client Element est déjà installé.

Dans l'accueil d'Element, on peut se connecter en précisant l'adresse de la machine de virtualisation `http://virt:9090` et les nom d'utilisateurs et mots de passe créés plus tôt.

En se connectant sur le même seveur sur deux machines, on peut alors commencer à discuter !