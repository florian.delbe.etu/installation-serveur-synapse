---
title: "SAE 3.03 - Déploiement d'une application"
subtitle: "Procédure des Sujets 1 et 2"
author: "Randy De Wancker et Florian Delbe"
---

# Configuration SSH

## 1. Première connexion

Selon la liste d'attribution des machines, Randy est attribué à acajou09 et Florian à bouleau07.
Pour se connecter à nos machines, nous utilisons respectivement ces commandes :

```
randy.dewancker.etu@phys$ ssh acajou09.iutinfo.fr
florian.delbe.etu@phys$ ssh bouleau07.iutinfo.fr
```
Nous avons vérifié l'empreinte des clés SSH et validé les connexions.

Nous avons ensuite créé une paire de clé avec la commande `ssh-keygen` et transmis la clé publique au serveur en utilisant cette commande :
```
$ ssh-copy-id -i .ssh/id_rsa.pub virtu
```

## 2. Création d'alias

Les alias permettent de se connecter plus rapidement à des machines en ssh.

### a. Connexion à la machine de virtualisation

Dans le fichier `~/.ssh/config`, nous ajoutons les lignes suivantes.
```
Host virt #nom de l'alias
  HostName acajou09.iutinfo.fr #adresse de la machine
  ForwargAgent yes #activer le transert d'agent
```
Le transfert d'agent permet de transférer la clé privée sur la machine où l'on se connecte sans devoir la réinstaller. Cela permet de se connecter à une autre machine depuis celle-ci.

### b. Connexion à la machine virtuelle

Dans le fichier de configuration, nous ajoutons ces lignes.
```
Host vm
  User user #nom d'utilisateur
  HostName 192.168.194.3 #adresse de la machine virtuelle
```

### c. Connexion à la vm directement depuis la machine physique

Dans le fichier de configuration, nous ajoutons ces lignes.
```
Host vmjump
  ProxyJump virt #utilise l'alias virt pour se connecter à la machine de virtualisation
  User user
  HostName 192.168.194.3 #connexion à la machine virtuelle
```

# Machine Virtuelle

## 1. Création de la machine

Une fois connecté à la machine de virtualisation, il est temps de créer la machine virtuelle.
Tout d'abord, on récupère le script de vmiut pour accéder aux commandes :
```
$ source /home/public/vm/vm.env
```

Ensuite nous créons la machine Matrix puis la démarrons avec les commandes :
```
$ vmiut make matrix
$ vmiut start matrix
```

Pour s'y connecter, on utilise l'une des deux commandes suivantes :
```
$ vmiut console matrix #nécessite d'avoir mit l'option -X lors de la connexion ssh
$ ssh user@192.168.194.25 #ip de départ de la machine virtuelle
```
*Précision : pour des raison de sécurité, on ne peut pas se connecter en ssh avec l'utilisateur `root`.*

## 2. Configuration réseau

Une fois connecté sur la machine virtuelle, nous voulons changer l'adresse ip en `192.168.194.3`.

Premièrement, il faut se connecter en `root` et couper l'interface avant toute modification (attention à être connecté avec la console et non en ssh, sinon la connexion se coupe) :
```
user@vm$ su --login root
root@vm# ifdown enp0s3
```

Nous configurons l'interface réseau avec la nouvelle adresse ip et le routeur `192.168.194.2` dans le fichier `/etc/network/interfaces` :
```
iface enp0s3 inet static
        address 192.168.194.3
        gateway 192.168.192.2
        netmask 255.255.255.0
```

Puis nous modifions l'adresse du serveur DNS dans le fichier `/etc/resolv.conf` en modifiant la ligne `nameserver 192.168.194.2`.

Enfin, nous rétablissons l'interface pour activer les modifications : `ifup enp0s3`

Nous pouvons vérifier la persistance de la configuration avec un `reboot`.
La configuration est correcte et persistante.

## 3. Configuration du serveur proxy

Pour se connecter à internet depuis le réseau privé, on passe par le serveur proxy de l'université.
Dans le fichier `/etc/environment`, on ajoute :
```
HTTP_PROXY=http://cache.univ-lille.fr:3128
HTTPS_PROXY=http://cache.univ-lille.fr:3128
http_proxy=http://cache.univ-lille.fr:3128
https_proxy=http://cache.univ-lille.fr:3128
NO_PROXY=localhost,192.168.194.0/24,172.18.48.0/22
```

On peut tester la connexion avec un `wget https://www.framasoft.org`

## 4. Mise à jour du système et installation d'outils

Pour mettre à jour le système, on execute la commande la commande suivante puis on redémarre la machine.
```
# apt update && apt full-upgrade
# reboot
```

On installe ensuite les outils `vim`, `less`, `tree` et `rsync` respectivement avec les commandes :
```
# apt-get install vim
# apt-get install less
# apt-get install tree
# apt-get install rsync
```
## 5. Changement du nom de machine

Nous changeons le nom de la machine virtuelle en `matrix` en modifiant deux fichiers :

- Le fichier `/etc/hostname` dans lequel nous remplaçons `debian` par `matrix`.
- Le fichier `/etc/hosts` dans lequel nous remplaçons la ligne `127.0.1.1   debian` par `127.0.1.1   matrix`.

Il est ensuite possible de faire un `ping matrix`.

## 6. Installation de la commande sudo

Le fichier `/etc/sudoers` donne les droits sudo aux utilisateurs du groupe "sudo".
Ainsi pour donner les droits sudo à un utilisateur, il suffit de l'ajouter au groupe "sudo" avec cette commande :
```
root@matrix# usermod user -g sudo
```

## 7. Configuration de la synchronisation d'horloge

Pour synchroniser l'heure de la machine virtuelle, nous utilisons ici le serveur NTP de l'université (`ntp.univ-lille.fr`).

D'abord, nous modifions le fichier `/etc/systemd/timesyncd.conf` dans lequel nous ajoutons `NTP=ntp.univ-lille.fr`.

Ensuite, il suffit de redémarrer le service de synchronisation du temps de la machine avec cette commande :
```
root@matrix# systemctl restart systemd-timesyncd.service
```
Et voilà, l'horloge de la machine est à l'heure !

## 8. Installation d'un serveur de base de données

Nous installons le service `postgresql` avec `# apt-get install postgresql`.

Pour configuer la base de données, il faut se connecter avec l'utilisateur postgres (`su --login postgres`).

Nous créons un utilisateur "matrix" propriétaire d'une nouvelle base de données "matrix".
```
postgres@matrix$ createuser -P matrix #l'option -P sert à préciser un mot de passe
Enter password for a new role: matrix

postgres@matrix$ createdb matrix -O matrix #l'option -O sert à préciser l'utilisateur propriétaire
```

Enfin, nous pouvons executer des requêtes dans cette base de données avec cette commande :
```
$ psql -h localhost -c 'REQUETE SQL' matrix matrix
#les deux derniers paramètres sont associés aux options -d (base de données) et -U (utilisateur)
```